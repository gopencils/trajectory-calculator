using UnityEngine;

public class Cursor : MonoBehaviour 
{
    public static Cursor instance;
    Vector3 offset;
    RaycastHit hit;

    private void Awake()
    {
        instance = this;
    }
    void Update () 
	{
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, float.MaxValue, 1 << LayerMask.NameToLayer("Ground")))
            {
                offset = new Vector3(CannonController.instance.transform.position.x - hit.point.x, CannonController.instance.transform.position.y - hit.point.y,  CannonController.instance.transform.position.z + 20 - hit.point.z);
            }
        }

        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, float.MaxValue, 1 << LayerMask.NameToLayer("Ground")))
            {
                //if(hit.point.z > CannonController.instance.transform.parent.position.z)
                transform.position = new Vector3(Mathf.Clamp(hit.point.x + offset.x, -13, 13), hit.point.y + offset.y, Mathf.Clamp(hit.point.z + offset.z, CannonController.instance.transform.position.z + 20, hit.point.z + offset.z));
            }
        }
	}
}
