using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Collections;

public class CannonInterface : MonoBehaviour 
{
    [SerializeField]
    Cursor targetCursor;

    [SerializeField]
    CannonController cannon;

    [SerializeField]
    Text timeOfFlightText;

    [SerializeField]
    float defaultFireSpeed = 35;

    [SerializeField]
    float defaultFireAngle = 45;

    private float initialFireAngle;
    private float initialFireSpeed;
    private bool useLowAngle;

    private bool useInitialAngle;
    public LineRenderer line;
    public Transform range;

    void Awake()
    {
        useLowAngle = false;

        initialFireAngle = defaultFireAngle;
        initialFireSpeed = defaultFireSpeed;

        useInitialAngle = false;
    }

    public float speed;
    float moveSpeed;
    int indexNum;
    Coroutine jumpCo;
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            line.enabled = true;
            if (useInitialAngle)
                cannon.SetTargetWithAngle(targetCursor.transform.position, initialFireAngle);
            else
                cannon.SetTargetWithSpeed(targetCursor.transform.position, initialFireSpeed, useLowAngle);
            indexNum = 0;
        }

        if (Input.GetButtonUp("Fire1") && !EventSystem.current.IsPointerOverGameObject())
        {
            //cannon.Fire();
            if(jumpCo != null)
                StopCoroutine(jumpCo);
            jumpCo = StartCoroutine(delayJump());
        }

        ////round lerp value down to int
        //indexNum = Mathf.FloorToInt(moveSpeed);
        ////increase lerp value relative to the distance between points to keep the speed consistent.
        //moveSpeed += speed / Vector3.Distance(ProjectileArc.instance.points3d[indexNum], ProjectileArc.instance.points3d[indexNum + 1]);
        ////and lerp
        //transform.position = Vector3.Lerp(ProjectileArc.instance.points3d[indexNum], ProjectileArc.instance.points3d[indexNum + 1], moveSpeed - indexNum);

        timeOfFlightText.text = Mathf.Clamp(cannon.lastShotTimeOfFlight - (Time.time - cannon.lastShotTime), 0, float.MaxValue).ToString("F3");
    }

    IEnumerator delayJump()
    {
        yield return new WaitForSeconds(0.01f);
        while (indexNum < line.positionCount)
        {
            while (transform.localPosition != line.GetPosition(indexNum))
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(0, line.GetPosition(indexNum).y, line.GetPosition(indexNum).z), 30 * Time.deltaTime);
                yield return null;
            }
            indexNum++;
            Debug.Log(indexNum);
        }
        transform.parent = null;
        line.enabled = false;
        yield return new WaitForSeconds(0.01f);
        line.transform.position = transform.position;
        line.transform.rotation = Quaternion.Euler(0, 0, 0);
        Cursor.instance.transform.localPosition = Vector3.zero;
        transform.parent = line.transform;
        CannonController.instance.firePoint.transform.position = transform.position;
        //CannonController.instance.turret.transform.position = transform.position;
    }

    public void SetInitialFireAngle(string angle)
    {
        initialFireAngle = Convert.ToSingle(angle);
    }

    public void SetInitialFireSpeed(string speed)
    {
        initialFireSpeed = Convert.ToSingle(speed);
    }

    public void SetLowAngle(bool useLowAngle)
    {
        this.useLowAngle = useLowAngle;
    }

    public void UseInitialAngle(bool value)
    {
        useInitialAngle = value;
    }
}
